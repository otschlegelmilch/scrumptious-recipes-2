from django.contrib import admin
from recipes.models import FoodItem, Ingredient, Measure, Recipe, Step


admin.site.register(Recipe)
admin.site.register(FoodItem)
admin.site.register(Ingredient)
admin.site.register(Measure)
admin.site.register(Step)
